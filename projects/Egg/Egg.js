"use strict";

/**
 * @description syntax error for the egg lang
 */
class SyntaxError extends Error {}

/**
 * @description parse a program and identify it's content
 * @param code String
 * @returns content of the parsed code
 */

class Parse {
    constructor(code) {
        let {expr,rest} = this.parseExpression(code);
        if (this.parseSpaces(rest).length > 0) {
            throw new SyntaxError("Unexpected text after program");
        }
        return expr;
    }

    /**
     * @description convert expression to object info
     * @param {code} code regular expression 
     * @returns expression object info
     */
    parseExpression(code) {
        code = this.parseSpaces(code) // skip and spaces in start of the code;
        let match, expr;
        if (match = /^"([^"]*)"/.exec(code)) { // this is a string value
            expr = { type: "value", value: match[1] }
        } else if (match = /^\d+\b/.exec(code)) { // this is a number value
            expr = { type: "value", value: Number.parseFloat(match[0]) }
        } else if (match = /^[^\(\)\s,#]+/.exec(code)) { // this is an operator 
            expr = { type: "word", name: match[0] }
        } else {
            throw new SyntaxError(` Unexpected syntax: "${code}"`)
        }
        return this.parseApply(expr, code.slice(match[0].length));
    }
    /**
     * @description recursive function identify string content 
     */
    parseApply(expr, code) {
        code = this.parseSpaces(code);
        if (code[0] != "(") { return { expr: expr, rest: code }; }
        code = this.parseSpaces(code.slice(1));
        expr = { type: "apply", operator: expr, args: [] };

        while (code[0] != ")") {
            let arg = this.parseExpression(code);
            expr.args.push(arg.expr);
            code = this.parseSpaces(arg.rest);
            if (code[0] == ",") {
                code = this.parseSpaces(code.slice(1));
            } else if (code[0] != ")") {
                throw new SyntaxError("Expected ',' or ')'");
            }
        }
        return this.parseApply(expr, code.slice(1));
    }

    // helper method to skip spaces in the string 
    parseSpaces(string) {
        let first = string.search(/\S/);
        if (first == -1) return "";
        return string.slice(first);
    }

    /**
     * @description evaluate and execute the code
     * @param expr Code string 
     * @param scope Object
     */
    static evaluate(expr, scope) {
        if (expr.type == "word") {
            if (expr.name in scope) {
                return scope[expr.name];
            } else {
                throw new ReferenceError(`Undefined binding: ${expr.name}`);
            }
        } else if (expr.type == "value") {
            return expr.value;
        } else if (expr.type == "apply") {
            let { operator, args } = expr;
            if (operator.type == "word" &&
                operator.name in specialForms) {
                return specialForms[operator.name](expr.args, scope);
            } else {
                let op = Parse.evaluate(operator, scope);
                if (typeof op == "function") {
                    return op(...args.map(arg => Parse.evaluate(arg, scope)));
                } else {
                    throw new TypeError("Applying a non-function.");
                }
            }
        }
    }
}


let specialForms = {
    if (args, scope) {
        if (args.length != 3) {
            throw new Error("the if method expect only 2 or 3 argument")
        } else if (Parse.evaluate(args[0], scope) == false) {
            return Parse.evaluate(args[2], scope);
        } else {
            return Parse.evaluate(args[1], scope)
        }
    },
    define(args, scope) {
        if (args.length != 2 || args[0].type != "word") {
            throw new SyntaxError("Incorrect use of define");
        }
        let value = Parse.evaluate(args[1], scope);
        scope[args[0].name] = value;
        return value;
    },
    do(args, scope) {
        let value = false;
        for (let arg of args) {
            value = Parse.evaluate(arg, scope);
        }
        return value;
    },
    while (args, scope) {
        if (args.length != 2) {
            throw new SyntaxError("Wrong number of args to while");
        }
        while (Parse.evaluate(args[0], scope) !== false) {
            Parse.evaluate(args[1], scope);
        }
        return false;
    },

}

let scope = Object.create(null);
scope.true = true;
scope.false = false;
scope.print = (...values) =>{
   for (const value of values) {
       console.log(value);
   }
    return values;

};
for (let op of ["+", "-", "*", "/", "==", "<", ">"]) {
    scope[op] = Function("a, b", `return a ${op} b;`);
}

let temp =[]
function printValues(obj) {
    let inTemp
    if (!temp.includes(obj)) {
        if (obj.args != undefined) {
            temp = obj.args
        }
    }else{
        inTemp = true
    }
    for (var key in obj) {
        if (typeof obj[key] === "object") {
            printValues(obj[key]);
        }
        else{
            if (obj[key] != "apply" && obj[key] != "word" && obj[key] != "value") {
                if (obj[key] in specialForms || obj[key] in scope) {
                 if (inTemp) {
                    console.log("\t"+"("+obj[key]+")");}
                    else{
                    console.log("(" + obj[key] + ")");
                    }           
                }else{
                    if (inTemp) {
                        console.log("\t" + obj[key] );
                    } else {
                        console.log(obj[key]);
                    }
                }
            }
            
        }
    }
}
const treeify = require('treeify');

// export the run function to import and  run the code
module.exports = function run(program) {
     let parser = new Parse(program);
     // remove type == word
    //  printValues(parser)
    console.log("\n-> Code Tree Visualization <-\n");
    
    console.log(
        treeify.asTree(parser, true,)
    );
    console.log("-> Code Execution <-\n");
    return (Parse.evaluate(parser, scope));

}