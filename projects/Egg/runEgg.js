const run = require("./Egg")
code = `
do(define(total, 0),
    define(count, 1),
    while ( < (count, 11),
        do(define(total, +(total, count)),
            define(count, +(count, 1)))),
    print("Total is: ",total))
`;

run(code)