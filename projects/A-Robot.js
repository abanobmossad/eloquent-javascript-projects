/* 
mail - delivery robot picking up and dropping off parcels.
*/

// the roads edges of the village 
const roads = [
    "Alice's House-Bob's House", "Alice's House-Cabin",
    "Alice's House-Post Office", "Bob's House-Town Hall",
    "Daria's House-Ernie's House", "Daria's House-Town Hall",
    "Ernie's House-Grete's House", "Grete's House-Farm",
    "Grete's House-Shop", "Marketplace-Farm",
    "Marketplace-Post Office", "Marketplace-Shop",
    "Marketplace-Town Hall", "Shop-Town Hall"
];

/**
 * @description representation of the roads in shape of object { place: [all places connected with it] }
 * @param edges the rouds array []
 * @returns graph representer for the road edges 
 */
function buildGraph(edges) {
    let graph = Object.create(null);

    function addEdge(from, to) {
        if (graph[from] == null) {
            graph[from] = [to];
        } else {
            graph[from].push(to);
        }
    }
    for (let [from, to] of edges.map(r => r.split("-"))) {
        addEdge(from, to);
        addEdge(to, from);
    }
    return graph;
}

const roadGraph = buildGraph(roads);


/** 
 * @description represent the current location of the robot and the location of parcels{thair-place , thair-address} 
 * @param robotPlace string represent robot current place
 * @param parcels string represent parcels current places and addresses
 */
class VillageState {
    constructor(robotPlace, parcels) {
        this.robotPlace = robotPlace;
        this.parcels = parcels;
    }

    /**
     * @description move the robot to another destination to deliver the parcels.
     *              check the destination is a valid road in -RoadGraph -
     *              move the parcels that robot carrying to it 's destination and deliver them 
     *              map for moving and filter for deliver
     *  @param destination string represent robot destination
     * @returns the same state if the destination is invalid
     * @returns a new state by changing the robot place to the destination and add new parcel location
     */
    moveRobot(destination) {

        if (roadGraph[this.robotPlace].includes(destination)) {
            let newParcels = this.parcels.map(parcel => {
                if (parcel.place != this.robotPlace) return parcel // apply moving to only parcels with the robot 
                return { // moving to destination with the same address 
                    place: destination,
                    address: parcel.address
                }
            }).filter(p => p.place != p.address) // remove delivered  

            return new VillageState(destination, newParcels)
        } else {
            console.log('The destination is not a valid road');
            return this
        }
    }

    /**
     * @static
     * @description generate a random state with random parcels places and  addresses
     * @param parcelCount Number of parcels to be generated 
     * @param initialPlace String fixed place
     * @returns a new state with generated parcels with fixed place
     */
    static fromRandom(parcelCount = 5, initialPlace = "Post Office") {
        let parcels = [];
        for (let i = 0; i < parcelCount; i++) {
            let address = randomPick(Object.keys(roadGraph));
            let place;
            do {
                place = randomPick(Object.keys(roadGraph));
            } while (place == address);
            parcels.push({
                place,
                address
            });
        }

        return new VillageState(initialPlace, parcels);
    }
}


/**
 * @description run simulation for the robot to moving  in the world and deliver all the parcels 
// @param villageState VillageState object 
// @param robotDirection robot direction object
*/
function runRobot(villageState, robotDirection) {
    let move = 0;
    while (true) {
        if (villageState.parcels.length == 0) {
            console.log(`Finished all parcels in ${move} move`);
            break;
        }
        let action = robotDirection(villageState);
        let direction = action.direction;

        villageState = villageState.moveRobot(direction);
        console.log(`Robot Moved to ${direction}`);

        move++;
    }

}

/**
 * 
 * @description make a robot with random destination to move to .
 * @param villageState VilageState object to pike a place 
 * @returns direction 
 */
function randomRobotDirection(villageState) {
    return {
        direction: randomPick(roadGraph[villageState.robotPlace])
    };
}


/**
 * @description helper method to choice item randomly from an array.
 * @param array array 
 * @returns a chosen item
 */
function randomPick(array) {
    let choice = Math.floor(Math.random() * array.length);
    return array[choice];
}


// simulate the robot work by deliver all the parcels to their addresses 
runRobot(VillageState.fromRandom(), randomRobotDirection);