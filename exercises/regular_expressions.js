"use strict";

let settings = `
name = Vasilis
[addresdfsdfsdss]
[addresdfsdss]
[addrsdfsess]
[address]
city = Tessaloniki 
`;

function parsIni(file) {
    let lines = file.split(/\r?\n/);
    let result = {}
    let section = result;
    lines.forEach(line => {
        let match;
        if (match = line.match(/^(\w+)\s?=\s?(.*)$/)) {
            section[match[1]] = match[2];
        } else if (match = line.match(/^\[(\w+)\]$/)) {
            result[match[1]] = {};
            section = result[match[1]];

        }
    });
    console.log(result);

}

// parsIni(settings)


// RegExp Golf

verify(/(car)|(cat)/,
    ["my car", "bad cats"],
    ["camper", "high art"]);

verify(/(pop)|(props)/,
    ["pop culture", "mad props"],
    ["plop", "prrrop"]);

verify(/(ferret)|(ferry)|(ferrari)/,
    ["ferret", "ferry", "ferrari"],
    ["ferrum", "transfer A"]);

verify(/ious\b/,
    ["how delicious", "spacious room"],
    ["ruinous", "consciousness"]);

verify(/\s\.|;|;/,
    ["bad punctuation ."],
    ["escape the period"]);

verify(/(\w{6,})/,
    ["hottentottententen"],
    ["no", "hotten totten tenten"]);

verify(/\b[^\se]+\b/,
    ["red platypus", "wobbling nest"],
    ["earth bed", "le"])

function verify(regexp, yes, no) {
    // Ignore unfinished exercises
    if (regexp.source == "...") return;
    for (let str of yes)
        if (!regexp.test(str)) {
            console.log(`Failure to match '${str}'`);
        } else {
            console.log(`${str} Right`);
        }
    for (let str of no)
        if (regexp.test(str)) {
            console.log(`Unexpected match for '${str}'`);
        }
}


let text = "'I'm the cook,' he said, 'it's my job.'";
// Change this call.
console.log(text.replace(/(\b'\b)/g, "__").replace(/'/g, "\"").replace(/__/g, "'"));
// → "I'm the cook," he said, "it's my job."